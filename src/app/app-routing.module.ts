import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { ResourcesComponent } from './component/resources/resources.component';
import { ResourceFrontComponent } from './component/resource-front/resource-front.component';

const routes: Routes = [
{
  path: "home", 
  component: HomeComponent,
},
{
  path: "resources", 
  component: ResourcesComponent,
},
{
  path: "articles", 
  component: ResourceFrontComponent,
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
