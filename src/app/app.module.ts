import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { HomeComponent } from './component/home/home.component';
import { ResourcesComponent } from './component/resources/resources.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { MenuBackOfficeComponent } from './component/menu-back-office/menu-back-office.component';
import { AccueilBackOfficeComponent } from './component/accueil-back-office/accueil-back-office.component';
import { StatsBackOfficeComponent } from './component/stats-back-office/stats-back-office.component';
import { GestionUtilisateurBackOfficeComponent } from './component/gestion-utilisateur-back-office/gestion-utilisateur-back-office.component';
import { GestionResourcesBackOfficeComponent } from './component/gestion-resources-back-office/gestion-resources-back-office.component';
import { CategoriesBackOfficeComponent } from './component/categories-back-office/categories-back-office.component';
import { CreationCategoriesBackOfficeComponent } from './component/creation-categories-back-office/creation-categories-back-office.component';
import { ListeCategoriesBackOfficeComponent } from './component/liste-categories-back-office/liste-categories-back-office.component';
import { ModifierCategoriesBackOfficeComponent } from './component/modifier-categories-back-office/modifier-categories-back-office.component';
import { BTNUtilisateurBackOfficeComponent } from './component/btn-utilisateur-back-office/btn-utilisateur-back-office.component';
import { ResourcesBackOfficeComponent } from './component/resources-back-office/resources-back-office.component';
import { ResourceFrontComponent } from './component/resource-front/resource-front.component';
import { RechercheBarreComponent } from './component/recherche-barre/recherche-barre.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ResourcesComponent,
    ConnexionComponent,
    MenuBackOfficeComponent,
    AccueilBackOfficeComponent,
    StatsBackOfficeComponent,
    GestionUtilisateurBackOfficeComponent,
    GestionResourcesBackOfficeComponent,
    CategoriesBackOfficeComponent,
    CreationCategoriesBackOfficeComponent,
    ListeCategoriesBackOfficeComponent,
    ModifierCategoriesBackOfficeComponent,
    BTNUtilisateurBackOfficeComponent,
    ResourcesBackOfficeComponent,
    ResourceFrontComponent,
    RechercheBarreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
