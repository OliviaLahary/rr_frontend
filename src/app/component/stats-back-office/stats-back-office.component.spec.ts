import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsBackOfficeComponent } from './stats-back-office.component';

describe('StatsBackOfficeComponent', () => {
  let component: StatsBackOfficeComponent;
  let fixture: ComponentFixture<StatsBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatsBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatsBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
