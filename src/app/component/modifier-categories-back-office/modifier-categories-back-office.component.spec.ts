import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierCategoriesBackOfficeComponent } from './modifier-categories-back-office.component';

describe('ModifierCategoriesBackOfficeComponent', () => {
  let component: ModifierCategoriesBackOfficeComponent;
  let fixture: ComponentFixture<ModifierCategoriesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifierCategoriesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifierCategoriesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
