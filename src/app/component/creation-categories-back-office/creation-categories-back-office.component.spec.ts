import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationCategoriesBackOfficeComponent } from './creation-categories-back-office.component';

describe('CreationCategoriesBackOfficeComponent', () => {
  let component: CreationCategoriesBackOfficeComponent;
  let fixture: ComponentFixture<CreationCategoriesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreationCategoriesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreationCategoriesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
