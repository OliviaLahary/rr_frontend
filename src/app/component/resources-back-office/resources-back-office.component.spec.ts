import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesBackOfficeComponent } from './resources-back-office.component';

describe('ResourcesBackOfficeComponent', () => {
  let component: ResourcesBackOfficeComponent;
  let fixture: ComponentFixture<ResourcesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourcesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResourcesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
