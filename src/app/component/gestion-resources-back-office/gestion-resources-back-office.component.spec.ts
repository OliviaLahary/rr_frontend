import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionResourcesBackOfficeComponent } from './gestion-resources-back-office.component';

describe('GestionResourcesBackOfficeComponent', () => {
  let component: GestionResourcesBackOfficeComponent;
  let fixture: ComponentFixture<GestionResourcesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionResourcesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GestionResourcesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
