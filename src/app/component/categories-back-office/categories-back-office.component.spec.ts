import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesBackOfficeComponent } from './categories-back-office.component';

describe('CategoriesBackOfficeComponent', () => {
  let component: CategoriesBackOfficeComponent;
  let fixture: ComponentFixture<CategoriesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoriesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
