import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BTNUtilisateurBackOfficeComponent } from './btn-utilisateur-back-office.component';

describe('BTNUtilisateurBackOfficeComponent', () => {
  let component: BTNUtilisateurBackOfficeComponent;
  let fixture: ComponentFixture<BTNUtilisateurBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BTNUtilisateurBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BTNUtilisateurBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
