import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionUtilisateurBackOfficeComponent } from './gestion-utilisateur-back-office.component';

describe('GestionUtilisateurBackOfficeComponent', () => {
  let component: GestionUtilisateurBackOfficeComponent;
  let fixture: ComponentFixture<GestionUtilisateurBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionUtilisateurBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GestionUtilisateurBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
