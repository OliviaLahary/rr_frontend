import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificationDeCompteComponent } from './modification-de-compte.component';

describe('ModificationDeCompteComponent', () => {
  let component: ModificationDeCompteComponent;
  let fixture: ComponentFixture<ModificationDeCompteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificationDeCompteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModificationDeCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
