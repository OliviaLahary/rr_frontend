import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeCategoriesBackOfficeComponent } from './liste-categories-back-office.component';

describe('ListeCategoriesBackOfficeComponent', () => {
  let component: ListeCategoriesBackOfficeComponent;
  let fixture: ComponentFixture<ListeCategoriesBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeCategoriesBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListeCategoriesBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
