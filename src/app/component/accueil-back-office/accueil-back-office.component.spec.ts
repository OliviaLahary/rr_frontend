import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilBackOfficeComponent } from './accueil-back-office.component';

describe('AccueilBackOfficeComponent', () => {
  let component: AccueilBackOfficeComponent;
  let fixture: ComponentFixture<AccueilBackOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilBackOfficeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccueilBackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
