import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceFrontComponent } from './resource-front.component';

describe('ResourceFrontComponent', () => {
  let component: ResourceFrontComponent;
  let fixture: ComponentFixture<ResourceFrontComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceFrontComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResourceFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
